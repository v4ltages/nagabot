// Filesystem manipulation
const fs = require('fs');

// Rest functions in discord.js
const { REST } = require('@discordjs/rest');
const { Routes } = require('discord-api-types/v9');

// Dotenv for auth and clientid
require('dotenv/config');

const commands = [];
const commandFiles = fs.readdirSync('./src/commands').filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
	const command = require(`./src/commands/${file}`);
	commands.push(command.data.toJSON());
}

const rest = new REST({ version: '9' }).setToken(process.env.DISCORD_TOKEN);

rest.put(Routes.applicationCommands(process.env.CLIENTID), { body: commands })
	.then(() => console.log('Successfully pushed application commands to Discord'))
	.catch(console.error);