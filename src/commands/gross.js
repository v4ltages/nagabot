const { SlashCommandBuilder } = require('@discordjs/builders');
const { grossarray } = require('../../utils');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('gross')
		.setDescription('So so gross...')
		.addUserOption(option => option.setName('mention').setDescription('Tell a user to say that there gross...')),
	async execute(interaction) {
		const user = interaction.options.getUser('mention');
		if (user) return interaction.reply(`${interaction.member} thats grossss... ${user}!\n ${grossarray[Math.floor(Math.random() * grossarray.length)]}`);
		interaction.reply(`${interaction.member} is saying that is so so gross. \n${grossarray[Math.floor(Math.random() * grossarray.length)]}`);
	},
};