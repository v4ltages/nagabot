const { MessageEmbed } = require('discord.js');
const { SlashCommandBuilder } = require('@discordjs/builders');
const { JsonDB } = require('node-json-db');
const { Config } = require ('node-json-db/dist/lib/JsonDBConfig');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('issue')
		.setDescription('For reporting issues with the bot. NOTICE: Ur User ID will be sent to the developer!')
		.addStringOption(option => option.setName('title').setDescription('Title of the issue').setRequired(true)).addStringOption(option => option.setName('description').setDescription('Description of the issue you experienced.').setRequired(true)),
	async execute(interaction) {
		const title = interaction.options.getString('title');
		const description = interaction.options.getString('description');
		const issue = new MessageEmbed()
			.addFields(
				{ name: 'Title:', value: title },
				{ name: 'Description:', value: description },
				{ name: 'By User:', value: `${interaction.user}` },
			)
			.setFooter({ text: 'Sent successfully to the developer.\nPlease consider sending any issues to https://gitlab.com/v4ltages/hayase/-/issues aswell!' });
		interaction.reply({ embeds: [issue], ephemeral: true });
		const db = new JsonDB(new Config('issueReports', true, true, '/'));
		db.push('/', { issues:[ { title:`${title}`, description:`${description}`, by_user:`${interaction.user}` }] }, false);
	},
};