// Utils contains the array.
const { virginarray } = require('../../utils');
const { SlashCommandBuilder } = require('@discordjs/builders');

const responceList = [
	'agrees!',
	'gives a thumbs up on that',
	'says its fine',
	'thinks thats cool',
];

module.exports = {
	data: new SlashCommandBuilder()
		.setName('thumbsup')
		.setDescription('Thats a thumbsup from me, Senpai.')
		.addUserOption(option => option.setName('mention').setDescription('Who exactly needs the attencion?'))
		.addStringOption(option => option.setName('input').setDescription('What you want it to say?')),
	async execute(interaction) {
		const user = interaction.options.getUser('mention');
		const input = interaction.options.getString('input');
		if (input) {
			if (user) {
				return interaction.reply(`${interaction.member} *${responceList[Math.floor(Math.random() * responceList.length)]}* ${user} ${input}\n${virginarray}`);
			}
			return interaction.reply(`${interaction.member} *gives a thumbs up.* ${input}\n${virginarray}`);
		}
		else if (user) {
			if (input) {
				return interaction.reply(`${interaction.member} *${responceList[Math.floor(Math.random() * responceList.length)]}* ${user} ${input}\n${virginarray}`);
			}
			return interaction.reply(`${interaction.member} *${responceList[Math.floor(Math.random() * responceList.length)]}* ${user}\n${virginarray}`);
		}
		else {
			return interaction.reply(`${interaction.member} *${responceList[Math.floor(Math.random() * responceList.length)]}*\n${virginarray}`);
		}
	},
};