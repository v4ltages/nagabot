// Utils contains the array.
const { virginarray } = require('../../utils');
const { SlashCommandBuilder } = require('@discordjs/builders');

const responceList = [
	'agrees!',
	'gives a thumbs up on that',
	'says its fine',
	'thinks thats cool',
];

const agreeList = responceList.slice(0, -3);

module.exports = {
	data: new SlashCommandBuilder()
		.setName('thup')
		.setDescription('The thumbsup command but with subcommands for different reactions.')
		.addSubcommand(subcommand =>
			subcommand
				.setName('cool')
				.setDescription('Thats cool')
				.addUserOption(option => option.setName('mention').setDescription('Select a user to tell them thats cool.')))
		.addSubcommand(subcommand =>
			subcommand
				.setName('agree')
				.setDescription('Agree with people')
				.addUserOption(option => option.setName('mention').setDescription('Select a user to agree with.'))),
	async execute(interaction) {
		const user = interaction.options.getUser('mention');
		if (interaction.options.getSubcommand() === 'cool') {
			if (user) {
				return interaction.reply(`${interaction.member} *${responceList[3]}* ${user}\n${virginarray}`);
			}
			return interaction.reply(`${interaction.member} *${responceList[3]}*\n${virginarray}`);
		}
		else if (interaction.options.getSubcommand() === 'agree') {
			if (user) {
				return interaction.reply(`${interaction.member} *${agreeList[Math.floor(Math.random() * agreeList.length)]}* ${user}\n${virginarray}`);
			}
			return interaction.reply(`${interaction.member} *${agreeList[Math.floor(Math.random() * agreeList.length)]}*\n${virginarray}`);
		}
		else {
			return interaction.reply(`${interaction.member} *${responceList[Math.floor(Math.random() * responceList.length)]}*\n${virginarray}`);
		}
	},
};